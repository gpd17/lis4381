> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Gabrielle Dunlap

### Project 2 Requirements: 


1. Clone assignment a4 files: 
2. Create server side validation
3. Add and test database connection to php file
4. Activate update and delete button

#### README.md file should include the following items:

* Screenshot of LIS 4381 p2 index page
* Screenshot of edit petstore 
* Screenshot of edit petstore error page
* Screenshot of home page
* Screenshot of edit petstore 
* Screenshot of RSS Feed

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:


|   *Screenshot of lis4381 prg2 page*:       |   *Screenshot of edit petstore pag*:       |
|:------------------------------------------:|:------------------------------------------:|
|![lis4381 p2 Screenshot](img/p2index.png)   |![editptstore Screenshot](img/editps.png)   |
|   *Screenshot of errorptstore page*:       |   *Screenshot of lis4381 home page*:       |
|:------------------------------------------:|:------------------------------------------:|
|![errorstore Screenshot](img/errorpt.png)   |![lis4381 hpg Screenshot](img/homepg.png)   |
|   *Screenshot of RSS FEED lnk page*:       |
|:------------------------------------------:|
|![RSS Feed 1 Screenshot](img/rssfeed.png)   |




#### Local 4381 link:

*Local 4381 Link:*
[Link](http://localhost/repos/lis4381/index.php)
