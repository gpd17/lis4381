> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Gabrielle Dunlap

### Assignment 3 Requirements: 


1. Create a database that displays relationships between pet stores, customers, and pets.
2. Develop two user interfaces: First User Interface-prompts user to enter number of tickets and concert choice, Second User Interface- screen reads price of the ticket

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

|   *Screenshot of ERD*:        |   *Screenshot of first interface*:                |
|:-----------------------------:|:-----------------------------:                    |              |![ERD Screenshot](img/erd.png) |![firstuser Screenshot](img/firstuser.png)         |

|   *Screenshot of ERD*:        |
|:-----------------------------:|
|![seconduser Screenshot](img/seconduser.png) |


#### MWB and mySQL links:

*a3 MWB link:*
[A3 MWB Link](a3.mwb)

*a3 mySQL link:*
[A3 mySQL Link](a3.sql)