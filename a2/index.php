<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="This tab in my online portfolio shows the skills I used to complete this assignment.">
	<meta name="author" content="Gabrielle Dunlap">
	<link rel="icon" href="img/favicon.png">

	<title>A2</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
<h3>Assignmet 2 Requirements</h3>
<ul>
        <li>Create a mobile recipe app using Android Studio</li>
        <li>Develop two user interfaces</li>
</ul>	
	
<?php include_once("css/include_css.php"); ?>	

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #666;
	padding-top: 50px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
  
}
.bs-example
{
  margin: 20px;
}
</style>

</head>
<body>

                <?php include_once("global/nav_global.php"); ?>
               
                <div class="container">
                        <div class="starter-template">
                                <div class="page-header">
                                        <?php include_once("global/header.php"); ?>    
                                </div>

                                <!-- Start Bootstrap Carousel  -->
                                <div class="bs-example">
                                        <div
                                                id="myCarousel"
                                                                class="carousel"
                                                                data-interval="1000"
                                                                data-pause="hover"
                                                                data-wrap="true"
                                                                data-keyboard="true"                   
                                                                data-ride="carousel">
                                               
                                 <!-- Carousel indicators -->
                                                <ol class="carousel-indicators">
                                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                                        <li data-target="#myCarousel" data-slide-to="2"></li>
                                                </ol>  
                                                <!-- Carousel items -->
                                                <div class="carousel-inner">

                                                        <div class="active item" style="background-color: #ffffff; background-size: cover;">
                                                                <div class="container">
                                                                <img src="img/firstuser.png" alt="First User"> 
                                                                        <div class="carousel-caption">
                                                                                <h3>First User Interface</h3>
                                                                                <p class="lead">Screenshot of running first user interface.</p>
                                                                                
                                                                        </div>
                                                                </div>
                                                        </div>                                 

                                                        <div class="item" style="background: url(img/code.png) no-repeat left center; background-size: cover;">
                                                        <img src="img/seconduser.png" alt="Second User">
                                                                <div class="carousel-caption">
                                                                        <h3>Second User Interface</h3>
                                                                        <p>Screenshot of running second user interface. </p>
                                                                                                                    
                                                                </div>
                                                        </div>

                
                                                       
                                                </div>
                                                <!-- Carousel nav -->
                                                <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                                </a>
                                                <a class="carousel-control right" href="#myCarousel" data-slide="next">
                                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                        </div>
                                </div>
                                <!-- End Bootstrap Carousel  -->

                                <!-- Placeholder for responsive table if needed.  -->
                                <div class="table-responsive">
                                        <table id="myTable" class="table table-striped table-condensed" >

                                                <!-- Code displaying PetStore data with Edit/Delete buttons goes here // -->

                                        </table>
                                </div> <!-- end table-responsive -->
                               
                                <?php
                                include_once "global/footer.php";
                                ?>

                        </div> <!-- end starter-template -->
                </div> <!-- end container -->

                <?php include_once("js/include_js.php"); ?>    

</body>
</html>
