> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Gabrielle Dunlap

### Assignment 2 Requirements: 


1. Create a mobile recipe app using Android Studio
2. Develop two user interfaces

#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of running application's first user interface*:

![First User Interface Screenshot](img/firstuser.png)

*Screenshot of running application's second user interface*:

![Second User Interface Screenshot](img/seconduser.png)

