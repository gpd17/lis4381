> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Gabrielle Dunlap

### Assignment 4 Requirements: 


1. Clone assignment starter files: 
2. Edit file structure
3. Add jquery validation
4. Edit carousel 
5. Create index php for A1, A2, A3, and P1

#### README.md file should include the following items:

* Screenshot of LIS 4381 Main Portal Page
* Screenshot of failed validation
* Screenshot of passed validation

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:


|   *Screenshot of lis4381 home page*:       |   *Screenshot of failed validation*:       |
|:------------------------------------------:|:------------------------------------------:|
|![homepageva Screenshot](img/homepg1.png)   |![passedvaild Screenshot](img/failed.png)   |
|   *Screenshot of failed validation*:       |   *Screenshot of passed validation*:       |
|:------------------------------------------:|:------------------------------------------:|
|![failedvald Screenshot](img/failed2.png)   |![passedvaild Screenshot](img/passed.png)   |


#### Local 4381 link:

*Local 4381 Link:*
[Link](http://localhost/repos/lis4381/index.php)
