> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Gabrielle Dunlap

### Assignment 5 Requirements: 


1. Clone assignment starter files: 
2. Create server side validation
3. Add and test database connection to php file

#### README.md file should include the following items:

* Screenshot of LIS 4381 a5 index page
* Screenshot of add petstore with error page


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:


|   *Screenshot of lis4381 ass5 page*:       |   *Screenshot of failed validation*:       |
|:------------------------------------------:|:------------------------------------------:|
|![assignpgn5 Screenshot](img/assnpg5.png)   |![failedvaild Screenshot](img/failed.png)   |


#### Local 4381 link:

*Local 4381 Link:*
[Link](http://localhost/repos/lis4381/index.php)
