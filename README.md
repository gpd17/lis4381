# LIS4381 - Mobile Web Application Development

## Gabrielle Dunlap

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Distributed Version Control with Git and Bitbucket
    - Development Installations
    - Chapter Questions Chs (1,2)

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create a mobile recipe app using Android Studio
    - Develop two user interfaces

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create a database that displays relationships between pet stores, customers, and pets.
    - Develop two user interfaces: First User Interface-prompts user to enter number of tickets and concert choice, Second User Interface- screen reads price of the ticket
    
4. [A4 README.md](a4/README.md "My A4 README.md file") 
    - Clone assignment starter files: 
    - Edit file structure
    - Add jquery validation
    - Edit carousel 
    - Create index php for A1, A2, A3, and P1  
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Clone assignment starter files: 
    - Create server side validation
    - Add and test database connection to php file

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Develop two user interfaces: First User Interface-displays photo of auther and button for details, Second User Interface- displays details about author
    - Create a launcher icon image and display it in both activities (screens), add background color to both activities, add border around image and button, Must add text shadow (button) background to both interfaces

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Clone assignment a4 files: 
    - Create server side validation
    - Add and test database connection to php file
    - Activate update and delete button