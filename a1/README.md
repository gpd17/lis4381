> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Gabrielle Dunlap

### Assignment 1 Requirements: 

Three Parts

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation
* Screenshot of running java Hello
* Scfreenshot of running Android Studio
* git commands with short descriptions
* Bitbucket rep links: a) this assignment and b) the completed tutorials above (bitbucketstationslocations and myteamquotes).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create a new local repository 
2. git status - list the files you need to add or commit and the files you have changed
3. git add - add files
4. git commit - commits changes to the head but not the master branch
5. git push - sends changes to the master branch
6. git pull - fetch and merge changes on the remote server to your working directory
7. git config- tells git who are you

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/gpd17/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kob16/myteamquotes/ "My Team Quotes Tutorial")
