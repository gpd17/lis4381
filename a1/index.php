<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="This tab in my online portfolio shows the skills I used to complete this assignment.">
	<meta name="author" content="Gabrielle Dunlap">
	<link rel="icon" href="img/favicon.png">

	<title>A1</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
<h3>Assignmet 1 Requirements</h3>
<ul>
	<li> Distributed Version Control with Git and Bitbucket</li>
	<li> Development Installations </li>
	<li> Chapter Questions (Chs 1,2) </li>
</ul>	
<h3>Git Commands with short description</h3>
<ul>
	<li>git init - create a new local repository </li>
	<li>git status - list the files you need to add or commit and the files you have changed</li>
	<li>git add - add files</li>
	<li>git commit - commits changes to the head but not the master branch</li>
	<li>git push - sends changes to the master branch</li>
	<li>git pull - fetch and merge changes on the remote server to your working directory</li>
	<li>git config- tells git who are you</li>
</ul>	
<?php include_once("css/include_css.php"); ?>	

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #666;
	padding-top: 50px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
  
}
.bs-example
{
  margin: 20px;
}
</style>

</head>
<body>

                <?php include_once("global/nav_global.php"); ?>
               
                <div class="container">
                        <div class="starter-template">
                                <div class="page-header">
                                        <?php include_once("global/header.php"); ?>    
                                </div>

                                <!-- Start Bootstrap Carousel  -->
                                <div class="bs-example">
                                        <div
                                                id="myCarousel"
                                                                class="carousel"
                                                                data-interval="1000"
                                                                data-pause="hover"
                                                                data-wrap="true"
                                                                data-keyboard="true"                   
                                                                data-ride="carousel">
                                               
                                 <!-- Carousel indicators -->
                                                <ol class="carousel-indicators">
                                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                                        <li data-target="#myCarousel" data-slide-to="2"></li>
                                                </ol>  
                                                <!-- Carousel items -->
                                                <div class="carousel-inner">

                                                        <div class="active item" style="background-color: #ffffff; background-size: cover;">
                                                                <div class="container">
                                                                <img src="img/ampps.png" alt="Ampps"> 
                                                                        <div class="carousel-caption">
                                                                                <h3>AMPPS</h3>
                                                                                <p class="lead">Screenshot of AMPPS running</p>
                                                                                
                                                                        </div>
                                                                </div>
                                                        </div>                                 

                                                        <div class="item" style="background: url(img/code.png) no-repeat left center; background-size: cover;">
                                                        <img src="img/jdk_install.png" alt="JDK Install">
                                                                <div class="carousel-caption">
                                                                        <h3>JDK Install</h3>
                                                                        <p>Screenshot of running java hello </p>
                                                                                                                    
                                                                </div>
                                                        </div>

                                                        <div class="item" style="background: url(img/code3.jpg) no-repeat left center; background-size: cover;">
                                                        <img src="img/android.png" alt="Android"> 
                                                                <div class="carousel-caption">
                                                                        <h3>Android Studio</h3>
                                                                        <p>Screenshot of Android Studio- My First App</p>
                                                                                                                                                                                     
                                                                </div>
                                                        </div>
                                                       
                                                </div>
                                                <!-- Carousel nav -->
                                                <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                                </a>
                                                <a class="carousel-control right" href="#myCarousel" data-slide="next">
                                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                        </div>
                                </div>
                                <!-- End Bootstrap Carousel  -->

                                <!-- Placeholder for responsive table if needed.  -->
                                <div class="table-responsive">
                                        <table id="myTable" class="table table-striped table-condensed" >

                                                <!-- Code displaying PetStore data with Edit/Delete buttons goes here // -->

                                        </table>
                                </div> <!-- end table-responsive -->
                               
                                <?php
                                include_once "global/footer.php";
                                ?>

                        </div> <!-- end starter-template -->
                </div> <!-- end container -->

                <?php include_once("js/include_js.php"); ?>    

</body>
</html>
